/*
 * Copyright (C) 2014 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package com.funtv.funtestmicandcamera;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.iflytek.mic.MIC;
import com.iflytek.mictest.AudioPlayer;
import com.iflytek.mictest.FarUsbDevice;
import com.iflytek.mictest.FarUsbDevice.IAuthenListener;
import com.iflytek.mictest.FarUsbDevice.IRecordListener;
import com.iflytek.mictest.FarUsbDevice.IUsbNotify;
import com.funtv.funtestmicandcamera.camera.CameraPreview;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;


/*
 * MainActivity class that loads MainFragment
 */
public class MainActivity extends Activity {
    private String TAG = "chenhai";//MainActivity.class.getSimpleName();
    /**
     * Called when the activity is first created.
     */
    private final int START_PLAY_AUDIO = 0x1001;
    private final int START_RECORD = 0x1002;
    private final int STOP_RECORD = 0x1003;
    private final int CHECK_START = 0x1004;
    private final int CHECK_FINISH = 0x1005;
    private final int START_AUTHEN = 0x1006;
//    private final String RECORD_PATH = Environment
//            .getExternalStorageDirectory().getAbsolutePath()
//            + File.separator
//            + "record.pcm";
    private final String RECORD_PATH = "/mnt/sdcard/record.pcm";

    private Context mContext;

    private AudioPlayer mAudioPlayer;
    private FarUsbDevice mFarUsbDevice;
    private IUsbNotify mIUsbNotify;


    private int mMicTestStatus;
    private final int MIC_TEST_INIT = 0x2000;
    private final int MIC_TEST_IN = 0x3000;
    private final int MIC_TEST_FAIL = 0x4000;
    private final int MIC_TEST_SUCCESS = 0x5000;
    private TextView mMicStatusView = null;
    private Handler mMaiHandler;
    private StringBuilder mContentBuf;
    private TextView mContentTV;
    private boolean isInTest;
    private boolean isUsbDevOK = false;
    private String version;

    CameraPreview cp;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mContext = MainActivity.this;

        PackageManager packageManager = getPackageManager();
        PackageInfo packInfo;
        try {
            packInfo = packageManager.getPackageInfo(getPackageName(), 0);
            version = packInfo.versionName;
        } catch (PackageManager.NameNotFoundException e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        }

        mMaiHandler = new Handler() {
            public void handleMessage(Message msg) {
                switch (msg.what) {
                    case START_PLAY_AUDIO:
                        refreshContent("开始播放测试音频.\n");
                        PlayerThread playerThread = new PlayerThread();
                        playerThread.start();
                        // 先播后录
                        sendEmptyMessageDelayed(START_RECORD, 3000);
                        break;
                    case START_RECORD:
                        RecordThread recordThread = new RecordThread();
                        recordThread.start();
                        break;
                    case STOP_RECORD:
                        refreshContent("录音结束.\n");
                        // 停止录音
                        mFarUsbDevice.stopRecord();
                        try {
                            mRecordFos.close();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        sendEmptyMessage(CHECK_START);
                        break;
                    case CHECK_START:
                        refreshContent("开始音频一致性检查.\n");
                        CheckThread checkThread = new CheckThread();
                        checkThread.start();
                        break;
                    case CHECK_FINISH:
                        isInTest = false;
                        mFarUsbDevice.closeConnect();
                        refreshContent("一致性检查完成.\n");
                        Log.d("chenhai", "ret :" + mMicTestStatus);
                        OnShow();
                        break;
                    case START_AUTHEN:
                        OnShow();
                        refreshContent("开始USB鉴权测试.\n");
                        AuthenThread authenThread = new AuthenThread();
                        authenThread.start();
                        break;
                    default:
                        break;
                }
            };
        };

        mIUsbNotify = new IUsbNotify() {

            @Override
            public void onNotify(int msg) {
                if (msg == FarUsbDevice.GET_USB_MANAGER_FAILED) {
                    refreshContent("获取USB服务失败.\n");
                    isUsbDevOK = false;
                } else if (msg == FarUsbDevice.GET_USB_MANAGER_OK) {
                    refreshContent("获取USB服务成功.\n");
                    isUsbDevOK = true;
                } else if (msg == FarUsbDevice.GET_USB_DEVICE_FAILED) {
                    refreshContent("没有检查到USB麦克风设备.\n");
                    isUsbDevOK = false;
                }
            }
        };

        IntentFilter filter = new IntentFilter();
        filter.addAction("android.hardware.usb.action.USB_STATE");
        filter.addAction("android.hardware.usb.action.USB_DEVICE_ATTACHED");
        filter.addAction("android.hardware.usb.action.USB_DEVICE_DETACHED");
        mContext.registerReceiver(mUsbReceive, filter);

        initViews();
        mContentBuf = new StringBuilder();
        refreshContent("当前版本号为:" + version + "\n");
        mAudioPlayer = new AudioPlayer();
        // 初始化USB录音
        mFarUsbDevice = new FarUsbDevice(mContext, mIUsbNotify);
        mMaiHandler.post(task);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mContext.unregisterReceiver(mUsbReceive);
    }

    private BroadcastReceiver mUsbReceive = new BroadcastReceiver()  {

        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            Log.d(TAG, "usb action:"+ action);

            if(action.equals("android.hardware.usb.action.USB_DEVICE_ATTACHED")
                    ||action.equals("android.hardware.usb.action.USB_DEVICE_DETACHED")
                    ||action.equals("android.hardware.usb.action.USB_STATE")){
                mMicTestStatus = MIC_TEST_INIT;
                OnShow();
            }
        }
    };

    public boolean OnShow() {
        mMicStatusView.setText(R.string.str_mic_test_status);
        String micShow;

        switch (mMicTestStatus){
            case MIC_TEST_SUCCESS:
                micShow = mContext.getString(R.string.str_mic_test_status, "PASS");
                mMicStatusView.setText(micShow);
                mMicStatusView.setBackgroundColor(Color.parseColor("#FFFF2F"));
               // SetTestSuccess();
                break;
            case MIC_TEST_IN:
                micShow = mContext.getString(R.string.str_mic_test_status, "Testing..");
                mMicStatusView.setText(R.string.str_mic_test_status);
                mMicStatusView.setText(micShow);
                mMicStatusView.setBackgroundColor(Color.parseColor("#A52A2A"));
                break;
            case MIC_TEST_FAIL:
                micShow = mContext.getString(R.string.str_mic_test_status, "Fail");
                mMicStatusView.setText(R.string.str_mic_test_status);
                mMicStatusView.setText(micShow);
                mMicStatusView.setBackgroundColor(Color.parseColor("#A52A2A"));
                break;
            case MIC_TEST_INIT:
            default:
                micShow = mContext.getString(R.string.str_mic_test_status, "Waiting..");
                mMicStatusView.setText(R.string.str_mic_test_status);
                mMicStatusView.setText(micShow);
                mMicStatusView.setBackgroundColor(Color.parseColor("#A52A2A"));
                break;
        }
        return false;
    }

    // 播音线程
    class PlayerThread extends Thread {
        @Override
        public void run() {
            InputStream in;
            try {
                in = mContext.getAssets().open("recordtest.pcm");
                byte[] buf = new byte[1024];
                mAudioPlayer.play();
                int len = 0;
                while ((len = in.read(buf)) != -1) {
                    mAudioPlayer.writeAudio(buf, 0, len);
                }
                mAudioPlayer.stop();
                mMaiHandler.sendEmptyMessage(STOP_RECORD);
                refreshContent("测试音频播放结束.\n");
            } catch (IOException e) {
                refreshContent("播放测试音频失败.\n");
                mMaiHandler.sendEmptyMessage(STOP_RECORD);
            }
        }
    }

    // 录音线程
    FileOutputStream mRecordFos = null;

    class RecordThread extends Thread {
        @Override
        public void run() {
            String usbVersion = mFarUsbDevice.getSerialNumber();
            refreshContent("USB固件版本号:" + usbVersion + ".\n");
            File file = new File(RECORD_PATH);
            if (file.exists())
                file.delete();
            try {
                mRecordFos = new FileOutputStream(file);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
            if (mRecordFos == null) {
                refreshContent("创建录音文件失败.\n");
                return;
            }
            refreshContent("创建录音文件" + RECORD_PATH + ".\n" + "开始录音.\n");
            mFarUsbDevice.startRecord(new IRecordListener() {

                @Override
                public void onData(byte[] buf, int len) {
                    try {
                        mRecordFos.write(buf);
                    } catch (IOException e) {
                        e.printStackTrace();
                        try {
                            mRecordFos.close();
                        } catch (IOException e1) {
                            e1.printStackTrace();
                        }
                    }

                }
            });
        }
    }

    // 一致性检查线程
    class CheckThread extends Thread {
        @Override
        public void run() {
            File file = new File(RECORD_PATH);
            int micResultCount = 0;
            try {
                FileInputStream fis = new FileInputStream(file);
                int len = fis.available();
                if (len <= 0) {
                    refreshContent("待检测的音频文为空.\n");
                    return;
                }
                byte[] buf = new byte[len];
                fis.read(buf);
                // 检查第一帧音频是否是序号1
                int channel = buf[1] & 0x0F;
                if (channel != 1) {
                    refreshContent("待检测的音频第一帧channel号不是1.\n");
                    return;
                }
                int[] ibuffer = bytesToInt(buf, len);
                int ret[] = MIC.recordTestWr(ibuffer, ibuffer.length - 1280);
                Log.d(TAG, "ret[6]:" + ret[6]);
                if (ret[6] != 0) {
                    refreshContent("录音测试失败，麦克风序号 ： " + 1 + ".\n");
                    mMicTestStatus = MIC_TEST_FAIL;
                } else {
                    refreshContent("麦克风 " + 1 + "测试通过.\n");
                    micResultCount++;
                }
                Log.d(TAG, "ret[2]:" + ret[2]);
                if (ret[2] != 0) {
                    refreshContent("录音测试失败，麦克风序号 ： " + 2 + ".\n");
                    mMicTestStatus = MIC_TEST_FAIL;
                } else {
                    refreshContent("麦克风 " + 2 + "测试通过.\n");
                    micResultCount++;
                }
                Log.d(TAG, "ret[5]:" + ret[5]);
                if (ret[5] != 0) {
                    refreshContent("录音测试失败，麦克风序号 ： " + 3 + ".\n");
                    mMicTestStatus = MIC_TEST_FAIL;
                } else {
                    refreshContent("麦克风 " + 3 + "测试通过.\n");
                    micResultCount++;
                }
                Log.d(TAG, "ret[1]:" + ret[1]);
                if (ret[1] != 0) {
                    refreshContent("录音测试失败，麦克风序号 ： " + 4 + ".\n");
                    mMicTestStatus = MIC_TEST_FAIL;
                } else {
                    refreshContent("麦克风 " + 4 + "测试通过.\n");
                    micResultCount++;
                }
                if (micResultCount == 4){
                    mMicTestStatus = MIC_TEST_SUCCESS;
                }
                mMaiHandler.sendEmptyMessage(CHECK_FINISH);
            } catch (FileNotFoundException e) {
                refreshContent("读取录音文件失败.\n");
            } catch (IOException e) {
                refreshContent("读取录音文件失败.\n");
            }
        }
    }

    // USB鉴权线程
    class AuthenThread extends Thread {
        @Override
        public void run() {
            mFarUsbDevice.startAuthen(new IAuthenListener() {

                @Override
                public void authenResult(boolean isPass) {
                    if (isPass) {
                        refreshContent("USB鉴权通过.\n");
                        mMaiHandler.sendEmptyMessage(START_PLAY_AUDIO);
                    } else {
                        refreshContent("USB鉴权失败.\n");
                    }
                }
            });
        }
    }

    private int[] bytesToInt(byte[] src, int length) {
        int value = 0, offset = 0, i = 0;
        int[] ret = new int[length / 4 + 1];
        while (offset < length && length - offset >= 4) {
            value = (int) ((src[offset] & 0xFF)
                    | ((src[offset + 1] & 0xFF) << 8)
                    | ((src[offset + 2] & 0xFF) << 16) | ((src[offset + 3] & 0xFF) << 24));
            offset += 4;
            ret[i] = value;
            i++;
        }
        return ret;
    }

    private void refreshContent(String appendContent) {
        mContentBuf.append(appendContent);
        mMaiHandler.post(new Runnable() {

            @Override
            public void run() {
                mContentTV.setText(mContentBuf.toString());
            }
        });
    }

    private void clearContent() {
        mContentBuf.delete(0, mContentBuf.length());
        mContentTV.setText(mContentBuf.toString());
    }

    private void initViews() {
        //mContentTV = (TextView) findViewById(R.id.content_tv);
//        findViewById(R.id.start_test_btn).setOnClickListener(this);
//        findViewById(R.id.exit_btn).setOnClickListener(this);

//        findViewById(R.id.camera_test_status);
//        findViewById(R.id.mic_test_status);
        mContentTV = (TextView) findViewById(R.id.content_tv);

//        LinearLayout rightLayout = (LinearLayout)findViewById(R.id.rigth_layout);
        LinearLayout leftLayout = (LinearLayout)findViewById(R.id.left_layout);
//
//        LinearLayout _LinearLayout = (LinearLayout)rightLayout;
//        View _view = View.inflate(mContext, R.layout.test_item_bar, null);
//        mTestStatusView = (TextView) _view.findViewById(R.id.tv_item_bar_name);

        mMicStatusView = (TextView) findViewById(R.id.mic_test_status);

        if (leftLayout.isInEditMode()){
            cp = (CameraPreview) findViewById(R.id.vv_video_surface_view);
        }
    }

    private Runnable task = new Runnable() {
        @Override
        public void run() {
            mMaiHandler.postDelayed(this, 5000);

            if ((mMicTestStatus != MIC_TEST_SUCCESS) && (!isInTest)){
                clearContent();
                // 上一次的先关掉
                mFarUsbDevice.closeConnect();
                boolean isOK = mFarUsbDevice.openUsbDevice();
                if (isOK) {
                    refreshContent("USB录音设备打开成功.\n");
                    isInTest = true;
                    mMaiHandler.sendEmptyMessage(START_AUTHEN);
                    mMicTestStatus = MIC_TEST_IN;
                } else {
                    refreshContent("USB录音设备打开失败.\n");
                }
            }
        }
    };
}
