/***********************************************************************
 * Module:  CAEJni.java
 * Author:  jianhuang3
 * Purpose: Defines the Class CAEJni
 ***********************************************************************/

package com.iflytek.cae.jni;

public final class CAEJni {

    private static boolean CAE_LOADED;

    static {
        try {
            System.loadLibrary("vitvcae");
            CAE_LOADED = true;

            DebugLog(false);
        } catch (Exception e) {
            CAE_LOADED = false;
        }
    }

    public static boolean isLoaded() {
        return CAE_LOADED;
    }

    public static class OutValues {
        public int outValInt;
    }

    /**
     * 设置是否打开jni日志。
     * 
     * @param showLog
     *            true打开，false关闭
     */
    public static native void DebugLog(boolean showLog);

    /**
     * 新建CAE引擎，生成的引擎句柄将存在obj的caeHandle字段中。
     * 
     * @param resPath
     *            唤醒资源路径
     * @param wakeupCbName
     *            唤醒回调函数名
     * @param audioCbName
     *            抛音频回调函数名
     * @param param
     *            传入的参数设置
     * @param obj
     *            回调函数所在对象
     * 
     * @return 0表示成功，否则为错误码
     * */
    public static native int CAENew(String resPath, String wakeupCbName,
            String audioCbName, String param, Object obj);

    /**
     * 从原始的96K采样率真的阵列音频中抽取一路16K采样率的音频。
     * 
     * @param inData
     *            96K音频数据
     * @param inDataLen
     *            输入音频长度（一般为一帧）
     * @param channel
     *            需要抽取哪一路音频数据（1~12）
     * @param outData
     *            16K音频数据
     * @param outValues
     *            用于输出变量，相当于C函数传入的指针
     * 
     * @return 成功返回0，否则为错误码
     */
    public static native int CAEExtract16K(byte[] inData, int inDataLen,
            int channel, byte[] outData, Object outValues);

    /**
     * 向CAE引擎写入音频。
     * 
     * @param handle
     *            CAE引擎句柄
     * @param audioData
     *            音频数据
     * @param dataLen
     *            写入长度
     * 
     * @return 0表示成功，否则为错误码
     * */
    public static native int CAEAudioWrite(int handle, byte[] audioData,
            int dataLen);

    public static native byte[] CAEGetInfo(int handle, int type);

    public static native int CAESetInfo(int handle, int type, byte[] data,
            int data_len);

    /**
     * 重CAE引擎，重新进入待唤醒状态。
     * 
     * @param handle
     *            CAE引擎句柄
     * 
     * @return 0表示成功，否则为错误码
     * */
    public static native int CAEReset(int handle);

    /**
     * 销毁CAE引擎。
     * 
     * @param handle
     *            CAE引擎句柄
     * 
     * @return 0表示成功，否则为错误码
     * */
    public static native int CAEDestroy(int handle);

    /**
     * 改变拾音波束方向，在唤醒成功之后调用。例如：在角度0的方位进行唤醒（此时beam为0），唤醒之后如需在90度方位拾音，
     * 则调用此函数并设置beam参数为1
     * 
     * @param handle
     *            CAE引擎句柄
     * @param beam
     *            音束，取值：0-2（4麦阵列）、0-3（5麦阵列）
     * 
     * @return 0表示成功，否则为错误码
     */
    public static native int CAESetRealBeam(int handle, int beam);

    public static native int CAERead16kAudio(byte[] paramArrayOfByte);

    public static native int CAESetWParam(int cae, byte[] paramName,
            byte[] value);

}