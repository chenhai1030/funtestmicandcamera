package com.iflytek.cae;

public interface CAEListener {

    /**
     * 唤醒回调。
     * 
     * @param jsonResult
     *            唤醒结果json字符串
     *            ，字段有：angle（角度）、channel（声道）、power（能量值）、CMScore（唤醒得分）、beam（波束）
     * */
    public void onWakeup(String jsonResult);

    /**
     * 音频回调，在唤醒成功之后通过该接口抛出16K采样率16bit编码PCM音频。
     * 
     * @param audioData
     *            音频数据
     * @param dataLen
     *            数据长度
     * @param param1
     *            保留作扩展用
     * @param param2
     *            保留作扩展用
     */
    public void onAudio(byte[] audioData, int dataLen, int param1, int param2);

    /**
     * 出错回调。
     * 
     * @param error
     *            引擎错误，内含错误码和描述
     **/
    public void onError(CAEError error);

}
