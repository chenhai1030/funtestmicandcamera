package com.iflytek.cae;

public class CAEError extends Exception {

    /**
	 * 
	 */
    private static final long serialVersionUID = 705579169594933666L;

    private int mErrorCode;
    private String mDescription;

    public CAEError(int errorCode, String description) {
        super(description);

        mErrorCode = errorCode;
        mDescription = description;
    }

    /**
     * 获取错误代码。
     * 
     * @return 错误代码
     */
    public int getErrorCode() {
        return mErrorCode;
    }

    /**
     * 获取错误描述。
     * 
     * @return 错误描述
     */
    public String getDescription() {
        return mDescription;
    }

}
