package com.iflytek.cae;

import org.json.JSONException;
import org.json.JSONObject;
import com.iflytek.cae.jni.CAEJni;
import com.iflytek.cae.jni.CAEJni.OutValues;
import com.iflytek.cae.util.log.DebugLog;

import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.text.TextUtils;
import android.util.Log;

/**
 * CAE引擎单例对象。
 * 
 * @author admin
 * 
 */
public class CAEEngine {
    private static CAEEngine instance;
    private static int caeHandle = 0;
    private boolean mIsWakeup = false;
    private CAEListener mListener;

    private final static int MSG_WAKEUP = 1;
    private final static int MSG_AUDIO = 2;
    private final static int MSG_ERROR = 3;
    // 唤醒门限
    public static String WAKE_UP_THRESHOLD = "ivw_threshold_1";
    // 回声消除开关
    public static String AEC_ENABLE = "aec_enable";

    private Handler mMainHandler = new Handler(Looper.getMainLooper()) {

        public void handleMessage(Message msg) {
            switch (msg.what) {
            case MSG_WAKEUP:
                String result = (String) msg.obj;
                if (null != mListener) {
                    mListener.onWakeup(result);
                }
                break;

            case MSG_AUDIO:
                Bundle bundle = (Bundle) msg.obj;
                byte[] audioData = bundle.getByteArray("audioData");
                int dataLen = bundle.getInt("dataLen");
                int param1 = bundle.getInt("param1");
                int param2 = bundle.getInt("param2");

                if (null != mListener) {
                    mListener.onAudio(audioData, dataLen, param1, param2);
                }
                break;

            case MSG_ERROR:
                CAEError e = (CAEError) msg.obj;
                if (null != mListener) {
                    mListener.onError(e);
                }
                break;
            default:
                break;
            }
        }

    };

    /**
     * 唤醒回调函数。
     * 
     * @param angle
     *            唤醒角度
     * @param channel
     *            声道
     * @param power
     *            能量值
     * @param CMScore
     *            唤醒得分
     * @param beam
     *            波束
     */
    protected void ivwCb(int angle, int channel, float power, int CMScore,
            int beam) {
        mIsWakeup = true;

        JSONObject json = new JSONObject();
        try {
            json.put("angle", angle);
            json.put("channel", channel);
            json.put("power", power);
            json.put("CMScore", CMScore);
            json.put("beam", beam);

            String result = json.toString();
            mMainHandler.obtainMessage(MSG_WAKEUP, result).sendToTarget();

            DebugLog.LogD("ivw result:" + result);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    protected void ivwCb_taoyun(int angle, int channel, float power,
            int CMScore, int beam, char[] param1) {
        mIsWakeup = true;

        JSONObject json = new JSONObject();
        try {
            json.put("angle", angle);
            json.put("channel", channel);
            json.put("power", power);
            json.put("CMScore", CMScore);
            json.put("beam", beam);

            String result = json.toString();
            mMainHandler.obtainMessage(MSG_WAKEUP, result).sendToTarget();

            DebugLog.LogD("ivw result:" + result);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    /**
     * 音频回调函数。
     * 
     * @param audioData
     *            音频数据
     * @param dataLen
     *            数据长度
     * @param param1
     *            保留作扩展用
     * @param param2
     *            保留作扩展用
     */
    protected void audioCb(byte[] audioData, int dataLen, int param1, int param2) {
        Log.d("YJS", "audio");
        Bundle bundle = new Bundle();
        bundle.putByteArray("audioData", audioData);
        bundle.putInt("dataLen", dataLen);
        bundle.putInt("param1", param1);
        bundle.putInt("param2", param2);

        mMainHandler.obtainMessage(MSG_AUDIO, bundle).sendToTarget();
    }

    private CAEEngine() {

    }

    /**
     * 创建引擎单例对象。
     * 
     * @param resPath
     *            通过{@link com.iflytek.cae.util.res.ResourceUtil}获取的唤醒资源路径
     * @return 成功则返回引擎对象，否则返回null
     */
    public static CAEEngine createInstance(String resPath) {
        if (TextUtils.isEmpty(resPath)) {
            instance = null;
            DebugLog.LogE("Ivw resouce path is null!");

            return null;
        }

        synchronized (CAEEngine.class) {
            if (null == instance) {
                instance = new CAEEngine();
                int ret = CAEJni.CAENew(resPath, "ivwCb", "audioCb", null,
                        instance);

                if (0 == ret) {
                    DebugLog.LogD("CAE engine create success. handle="
                            + caeHandle);
                } else {
                    DebugLog.LogE("CAE engine create fail. error=" + ret);
                    instance = null;
                    caeHandle = 0;
                }
            } else {
                DebugLog.LogD("CAE engine has already existed!");
            }
        }

        return instance;
    }

    /**
     * 获取CAE引擎单例对象。
     * 
     * @return CAE引擎单例对象，未创建则返回null
     */
    public static CAEEngine getInstance() {
        return instance;
    }

    /**
     * 设置状态监听器，用于获取唤醒结果、16K音频数据和引擎错误。
     * 
     * @param listener
     *            监听器
     */
    public void setCAEListener(CAEListener listener) {
        mListener = listener;
    }

    private OutValues mExtractOutValues = new OutValues();

    /**
     * 从12路32bit、16K采样率单麦数据组合而成阵列音频（码率：12288bytes/16ms）中抽取1路16bit、16K采样率音频（码率：
     * 512bytes/16ms）。
     * <p>
     * <i>注：该函数是比较耗时的同步操作，最好在新线程中调用。</i>
     * 
     * @param inData
     *            阵列音频数据
     * @param inDataLen
     *            数据长度
     * @param channel
     *            需要抽取哪一路音频数据（1~12）
     * @param outData
     *            16K音频存储区
     * @return 16K音频数据长度，出错返回0
     */
    public int extract16K(byte[] inData, int inDataLen, int channel,
            byte[] outData) {
        if (0 == caeHandle) {
            DebugLog.LogE("CAE engine is destroyed, invalid call!");
            return 0;
        }

        int ret = CAEJni.CAEExtract16K(inData, inDataLen, channel, outData,
                mExtractOutValues);

        if (0 != ret) {
            mMainHandler.obtainMessage(MSG_ERROR, new CAEError(ret, ""))
                    .sendToTarget();
            return 0;
        }

        return mExtractOutValues.outValInt;
    }

    /**
     * 写入由12路32bit、16K采样率的单麦数据组合而成阵列录音数据，码率为12288bytes/16ms。<br>
     * 计算方法：12 * 16000 / 1000 * 4 * 16ms = 12288bytes
     * 
     * @param audioData
     *            音频数据
     * @param dataLen
     *            数据长度
     */
    public void writeAudio(byte[] audioData, int dataLen) {
        synchronized (CAEEngine.class) {
            if (0 != caeHandle) {
                int ret = CAEJni.CAEAudioWrite(caeHandle, audioData, dataLen);

                if (0 != ret) {
                    mMainHandler
                            .obtainMessage(MSG_ERROR, new CAEError(ret, ""))
                            .sendToTarget();

                    DebugLog.LogE("Write audio error. error=" + ret);
                    destroy();
                }
            } else {
                DebugLog.LogE("CAE engine is destroyed, invalid call!");
            }
        }
    }

    public int setInfo(byte[] audioData, int dataLen) {
        return CAEJni.CAESetInfo(caeHandle, 0, audioData, dataLen);
    }

    public byte[] getInfo() {
        return CAEJni.CAEGetInfo(caeHandle, 0);
    }

    /**
     * 重置CAE引擎，进入待唤醒状态。
     */
    public void reset() {
        synchronized (CAEEngine.class) {
            if (0 != caeHandle) {
                int ret = CAEJni.CAEReset(caeHandle);

                if (0 != ret) {
                    mMainHandler
                            .obtainMessage(MSG_ERROR, new CAEError(ret, ""))
                            .sendToTarget();

                    DebugLog.LogE("CAE engine reset fail. error=" + ret);
                    destroy();
                } else {
                    mIsWakeup = false;

                    DebugLog.LogD("CAE engine reset sucess.");
                }
            } else {
                DebugLog.LogE("CAE engine is destroyed, invalid call!");
            }
        }
    }

    /**
     * CAE引擎是否处于唤醒状态。
     * 
     * @return 是否已唤醒
     */
    public boolean isWakeup() {
        synchronized (CAEEngine.class) {
            if (0 == caeHandle) {
                mIsWakeup = false;

                DebugLog.LogE("CAE engine is destroyed, invalid call!");
            }
        }

        return mIsWakeup;
    }

    /**
     * 改变拾音波束方向，在唤醒成功之后调用。例如：在角度0的方位进行唤醒（此时beam为0），唤醒之后如需在90度方位拾音，
     * 则调用此函数并设置beam参数为1。 调用reset()之后，beam的设置会还原。
     * 
     * @param beam
     *            音束，取值：0-2（4麦阵列）、0-3（5麦阵列）
     */
    public void setRealBeam(int beam) {
        int ret = 0;

        synchronized (CAEEngine.class) {
            if (0 == caeHandle) {
                DebugLog.LogE("CAE engine is destroyed, invalid call!");
                return;
            }

            if (!mIsWakeup) {
                DebugLog.LogE("CAE engine is not awake, invalid call!");
                return;
            }

            ret = CAEJni.CAESetRealBeam(caeHandle, beam);
        }

        if (0 != ret) {
            DebugLog.LogD("CAE engine setRealBeam fail. error=" + ret);

            mMainHandler.obtainMessage(MSG_ERROR, new CAEError(ret, ""))
                    .sendToTarget();
        }
    }

    /**
     * 设置唤醒门限
     */
    public void setCAEParams(String name, String value) {
        int ret = CAEJni.CAESetWParam(caeHandle, name.getBytes(),
                value.getBytes());
    }

    /**
     * 销毁CAE引擎，此后getInstance()方法将返回null。
     */
    public void destroy() {
        if (0 == caeHandle) {
            return;
        }

        int ret = 0;

        synchronized (CAEEngine.class) {
            ret = CAEJni.CAEDestroy(caeHandle);

            if (0 == ret) {
                caeHandle = 0;
                instance = null;

                DebugLog.LogD("CAE engine destroy sucess.");
            }
        }

        if (0 != ret) {
            DebugLog.LogD("CAE engine destroy fail. error=" + ret);

            mMainHandler.obtainMessage(MSG_ERROR, new CAEError(ret, ""))
                    .sendToTarget();
        }
    }

}
