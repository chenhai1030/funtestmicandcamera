package com.iflytek.cae;

public class Version {

    /**
     * 返回SDK版本
     * 
     * @return 版本
     */
    public static String getVersion() {
        return "1.0";
    }

}
