package com.iflytek.cae.util.log;

public class DebugLog {

    private static String _TAG = "CAELog";

    /**
     * 是否允许打印CAE内部测试日志
     */
    private static boolean DEBUG_MODE = false;

    private static boolean SHOW_CAE_LOG = false;

    public static void setShowLog(boolean show) {
        SHOW_CAE_LOG = show;
    }

    /**
     * 设置log TAG
     * 
     * @param tag
     *            日志tag
     */
    public static void setTag(String tag) {
        _TAG = tag;
    }

    public static void LogD(String tag, String log) {
        if (SHOW_CAE_LOG)
            android.util.Log.d(tag, log);

    }

    public static void LogD(String log) {
        if (SHOW_CAE_LOG)
            android.util.Log.d(_TAG, log);

    }

    public static void LogE(String log) {
        if (SHOW_CAE_LOG)
            android.util.Log.e(_TAG, log);

    }

    public static void LogE(String tag, String log) {
        if (SHOW_CAE_LOG)
            android.util.Log.e(tag, log);

    }

    /**
     * msc私有日志，正式版本不会进行打印
     * 
     * @param log
     */
    public static void LogS(String log) {
        if (SHOW_CAE_LOG && DEBUG_MODE)
            android.util.Log.d(_TAG, log);
    }

    public static void LogS(String tag, String log) {
        if (SHOW_CAE_LOG && DEBUG_MODE)
            android.util.Log.d(tag, log);
    }

}
