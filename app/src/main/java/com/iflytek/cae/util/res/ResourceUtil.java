package com.iflytek.cae.util.res;

import java.io.File;

import android.content.Context;
import android.content.res.AssetFileDescriptor;
import android.text.TextUtils;

/**
 * 资源目录转换工具类，将控件对外开放的资源路径转换为内部路径格式。
 * 
 * 文件目录格式说明如下，支持复数形式： fo|file_info|offset|length;
 * 其中fd表示文件标识符，通过assets、res读取时传递，fo表示文件路径方式
 * 注意：每次通过assets、res获取的文件路径，都会生成一个文件句柄，造成资源泄露，需要在native中进行释放
 * 
 * @author iflytek
 */
public class ResourceUtil {

    public enum RESOURCE_TYPE {
        assets, // 资源在assets目录下
        res, // 资源在res目录下
        path
        // 资源在SD卡中
    }

    /**
     * 外部存储上资源存放的绝对路径。
     * 
     * @return 内部资源路径
     */
    public static String generateResourcePath(Context context,
            RESOURCE_TYPE type, String path) {
        String ret = null;
        if (type == RESOURCE_TYPE.path) {
            // path绝对路径读取
            ret = getAbsolutePath(path);
        } else {
            // assets、res目录下读取
            ret = getAssetOrResPath(context, type, path);
        }
        return ret;
    }

    /**
     * 返回assets目录下的资源路径。
     * 
     * @param context
     *            上下文
     * @param type
     *            路径类型
     * @param id
     *            assets下为资源文件名，res下为资源id
     * 
     * @return asset或res类型的内部资源路径
     */
    private static String getAssetOrResPath(Context context,
            RESOURCE_TYPE type, String id) {
        if (TextUtils.isEmpty(id) || context == null)
            return null;
        String path = context.getPackageResourcePath();
        AssetFileDescriptor des = null;
        String ret = null;
        long offset = 0;
        long length = 0;
        try {
            if (type == RESOURCE_TYPE.assets) {
                des = context.getAssets().openFd(id);
                offset = des.getStartOffset();
                length = des.getLength();
            } else {
                int resid = Integer.valueOf(id);
                des = context.getResources().openRawResourceFd(resid);
                offset = des.getStartOffset();
                length = des.getLength();
            }

            ret = "fo|" + path + "|" + offset + "|" + length;
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (des != null) {
                    des.close();
                    des = null;
                }
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        }
        return ret;
    }

    /**
     * 返回绝对路径下的资源路径。
     * 
     * @param context
     *            上下文
     * @param path
     *            资源绝对路径
     * 
     * @return path类型的内部资源路径
     */
    private static String getAbsolutePath(String path) {
        if (TextUtils.isEmpty(path))
            return null;

        File f = new File(path);
        if (!f.exists() || !f.isFile())
            return null;
        long length = f.length();
        long offset = 0;
        return "fo|" + path + "|" + offset + "|" + length;
    }

}