package com.iflytek.mictest;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Iterator;

import tv.yuyin.utility.Crc16;

import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.hardware.usb.UsbDevice;
import android.hardware.usb.UsbDeviceConnection;
import android.hardware.usb.UsbEndpoint;
import android.hardware.usb.UsbManager;
import android.os.Handler;
import android.util.Log;

import com.iflytek.cae.CAEEngine;
import com.iflytek.cae.util.res.ResourceUtil;
import com.iflytek.cae.util.res.ResourceUtil.RESOURCE_TYPE;

public class FarUsbDevice {
    private String TAG = FarUsbDevice.class.getSimpleName();
    private final String ACTION_USB_PERMISSION = "com.iflytek.request.USB_PERMISSION";
    private final int REQUESTTYPE_IN = 0x80;
    private final int REQUEST_GETDESCRIPTOR = 0x06;
    private int VALUE_SERIALNUMBER = 0x0303;
    private final int BUF_LEN = 480 * 30;
    private UsbManager mUsbManager;
    private UsbDevice mUsbDevice;
    private UsbDeviceConnection mRecConnection;
    private CAEEngine mCaeEngine;
    private boolean isStart;
    private IUsbNotify mIUsbNotify;
    private Context mContext;
    private Handler mUsbHandler;
    public static int GET_USB_MANAGER_FAILED = 0x2001;
    public static int GET_USB_MANAGER_OK = 0x2002;
    public static int GET_USB_DEVICE_FAILED = 0X2003;

    public FarUsbDevice(Context context, IUsbNotify iUSNotify) {
        mIUsbNotify = iUSNotify;
        mContext = context;

        mUsbManager = (UsbManager) context
                .getSystemService(Context.USB_SERVICE);
        if (mUsbManager == null) {
            mIUsbNotify.onNotify(GET_USB_MANAGER_FAILED);
        } else {
            String resPath = ResourceUtil.generateResourcePath(context,
                    RESOURCE_TYPE.assets, "dingdong.jet");
            mCaeEngine = CAEEngine.createInstance(resPath);
            mIUsbNotify.onNotify(GET_USB_MANAGER_OK);
        }
    }

    public boolean openUsbDevice() {
        HashMap<String, UsbDevice> deviceHashMap = mUsbManager.getDeviceList();
        Iterator<UsbDevice> iterator = deviceHashMap.values().iterator();
        while (iterator.hasNext()) {
            UsbDevice usbdvc = iterator.next();
            Log.e(TAG,
                    "pid=" + usbdvc.getProductId() + " vid="
                            + usbdvc.getVendorId());
            if (usbdvc.getProductId() == 0x5020
                    && usbdvc.getVendorId() == 0x0416) {
                Log.e(TAG, "find Device");
                mUsbDevice = usbdvc;
                break;
            }
        }
        if (mUsbDevice == null) {
            Log.d(TAG, "usbDevice == null");
            mIUsbNotify.onNotify(GET_USB_DEVICE_FAILED);
            return false;
        } else {
            mRecConnection = mUsbManager.openDevice(mUsbDevice);
            if (mRecConnection == null)
                return false;
            return true;
        }
    }

    public String getSerialNumber() {
        int length = getDescriptorLength(mRecConnection, REQUESTTYPE_IN,
                REQUEST_GETDESCRIPTOR, VALUE_SERIALNUMBER, 0);
        if (length <= 0)
            return null;
        byte[] buffer = new byte[length];
        int readret = controlTransfer(mRecConnection, REQUESTTYPE_IN,
                REQUEST_GETDESCRIPTOR, VALUE_SERIALNUMBER, 0x0000, buffer,
                buffer.length);
        Log.d(TAG, "getSerialNumber ret:" + readret);
        byte[] unicode = new byte[buffer[0] - 2];
        System.arraycopy(buffer, 2, unicode, 0, buffer[0] - 2);
        return getStringFromUnicode(unicode, 0, unicode.length);
    }

    private int getDescriptorLength(UsbDeviceConnection conn, int requestType,
            int request, int value, int index) {
        byte[] buf = new byte[2];
        try {
            conn.controlTransfer(requestType, request, value, index, buf, 2,
                    500);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return buf[0];
    }

    private int controlTransfer(UsbDeviceConnection conn, int requestType,
            int request, int value, int index, byte[] buffer, int length) {
        int ret = 0;
        try {
            ret = conn.controlTransfer(requestType, request, value, index,
                    buffer, length, 500);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return ret;
    }

    private String getStringFromUnicode(byte[] unicode, int offset, int length) {
        byte[] newunicode = new byte[length + 2];
        newunicode[0] = (byte) 0xff;
        newunicode[1] = (byte) 0xfe;
        System.arraycopy(unicode, 0, newunicode, 2, length);
        String string = "";
        try {
            string = new String(newunicode, "UNICODE");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return string;
    }

    //
    public void startRecord(IRecordListener iRecordListener) {
        UsbEndpoint recordInEndpoint = mUsbDevice.getInterface(0)
                .getEndpoint(0);
        isStart = true;
        boolean isFirst = true;
        while (isStart) {
            byte[] bytes = new byte[BUF_LEN];
            int ret = mRecConnection.bulkTransfer(recordInEndpoint, bytes,
                    bytes.length, 0);
            if (ret == BUF_LEN) {
                // 对齐,第一帧保证是channel1
                if (isFirst) {
                    int offset = 0;
                    for (int i = 0; i < BUF_LEN; i += 4) {
                        int channel = bytes[i + 1] & 0x0F;
                        if (channel == 1) {
                            offset = i;
                            break;
                        }
                    }
                    byte[] tbuf = new byte[BUF_LEN - offset];
                    System.arraycopy(bytes, offset, tbuf, 0, BUF_LEN - offset);
                    iRecordListener.onData(tbuf, BUF_LEN - offset);
                    isFirst = false;
                } else {
                    iRecordListener.onData(bytes, BUF_LEN);
                }
            } else {
                Log.e(TAG, "read usb buf len:" + ret);
            }

        }

    }

    public void stopRecord() {
        isStart = false;
    }

    // 加密芯片鉴权
    public void startAuthen(IAuthenListener iAuthenListener) {
        UsbEndpoint authInEndpoint = mUsbDevice.getInterface(1).getEndpoint(0);
        UsbEndpoint authOutEndpoint = mUsbDevice.getInterface(1).getEndpoint(1);
        mRecConnection.claimInterface(mUsbDevice.getInterface(1), true);
        int random = (int) (Math.random() * 100);
        byte[] bytes = new byte[64];
        for (int i = 0; i < 3; i++) {
            byte[] randkey = mCaeEngine.getInfo();
            byte[] senddatas = getsendData(random, randkey);
            int outret = mRecConnection.bulkTransfer(authOutEndpoint,
                    senddatas, senddatas.length, 1000);
            Log.d(TAG, "authenticationThread outret=" + outret);
            int inret = mRecConnection.bulkTransfer(authInEndpoint, bytes,
                    bytes.length, 1000);
            Log.d(TAG, "authenticationThread inret=" + inret);
            byte[] resultdatas = new byte[32];
            System.arraycopy(bytes, 9, resultdatas, 0, 32);
            if (inret == 64 && bytes[5] == (byte) random) {
                int caeret = mCaeEngine.setInfo(resultdatas, 32);
                Log.d(TAG, "caeret:" + caeret);
                if (caeret == 0) {
                    iAuthenListener.authenResult(true);
                    return;
                }

            }
        }
        iAuthenListener.authenResult(false);
    }

    private byte[] getsendData(int random, byte[] randkey) {
        byte[] head = { 0x49, 0x46, 0x4c, 0x59 };
        Crc16 crc16 = new Crc16();
        // byte[] randkey = mCaeEngine.getInfo();
        int i = 0;
        int j = 0;
        int count = 1 + 1 + 1 + 2 + 32 + 2;
        byte[] abyts = new byte[40];
        abyts[i++] = 0x03;
        abyts[i++] = (byte) count;
        abyts[i++] = 0x08;
        abyts[i++] = 0;
        abyts[i++] = (byte) (randkey[0] & 0xF & 0xFF);
        abyts[i++] = (byte) (((randkey[0] & 0xF) >> 8) & 0xFF);
        for (; j < 8; ++j) {
            abyts[i++] = randkey[j];
        }
        for (j = 8; j < 32; ++j) {
            abyts[i++] = 0;
        }
        byte[] tbytes = crc16.calc(abyts, 1, i - 1);
        abyts[i++] = tbytes[0];
        abyts[i++] = tbytes[1];
        // ///////////封装usb格式
        byte[] datas = new byte[64];
        System.arraycopy(head, 0, datas, 0, 4);
        datas[4] = 56;
        datas[5] = (byte) random;
        datas[6] = 1;
        datas[7] = 40;
        System.arraycopy(abyts, 0, datas, 8, abyts.length);
        int sum = 0;
        for (int index = 0; index < 56; index++)
            sum += (datas[index] & 0xFF);
        Log.d(TAG, "sum=" + sum);
        datas[56] = (byte) (sum & 0xFF);
        datas[57] = (byte) (sum >> 8 & 0xFF);
        datas[58] = (byte) (sum >> 16 & 0xFF);
        datas[59] = (byte) (sum >> 24 & 0xFF);
        return datas;
    }

    public void closeConnect() {
        if (mRecConnection != null) {
            mRecConnection.close();
        }
    }

    public interface IRecordListener {
        public void onData(byte[] buf, int len);
    }

    public interface IAuthenListener {
        public void authenResult(boolean isPass);
    }

    public interface IUsbNotify {
        public void onNotify(int msg);
    }

}
